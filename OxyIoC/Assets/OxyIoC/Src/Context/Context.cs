﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using OxyIoC.Signals;

namespace OxyIoC
{
	public class Context : IContext
	{
		private Dictionary<Type, Mapping> map = new Dictionary<Type, Mapping> ();
		private Dictionary<IBaseSignal, CommandMapping> commandsMap = new Dictionary<IBaseSignal, CommandMapping> ();
		private List<IContextLaunchObserver> launchAnticipants;
	
		private Injector injector;
		private CommandsPool commandsPool;
		private bool isLaunched = false;

		public Context ()
		{
			injector = new Injector (this);
			commandsPool = new CommandsPool();
		}

		#region IContext implementation

		private event ContextInitializationHandler _OnInitialized = delegate { };

		public event ContextInitializationHandler OnInitialized {
			add {
				if (isLaunched) {
					// Trigger callback immediately
					value.Invoke (this);
				} else {
					if (!_OnInitialized.GetInvocationList ().Contains (value)) {
						_OnInitialized += value;
					}
				}
			}
			remove {
				_OnInitialized -= value;
			}
		}

		public void Launch ()
		{
			isLaunched = true;
			PostInitialization ();
			if (_OnInitialized != null) {
				_OnInitialized.Invoke (this);
				_OnInitialized = delegate {
				};
			}

			// Anticipants
			if (launchAnticipants != null)
			{
				foreach (var anticipant in launchAnticipants)
				{
					anticipant.OnContextLaunched();
				}
				launchAnticipants = null;
			}
		}

		public T MapSingleton<T> () where T : class, new()
		{
			return MapSingleton<T, T> ();
		}

		public I MapSingleton<I, C> () where I : class where C : class, I, new()
		{
			Type tInterface = typeof(I);
			Type tClass = typeof(C);
			if (map.ContainsKey (tInterface)) {
				throw new IncorrectMappingException (String.Format ("Type {0} is already mapped to {1}. Mapping to {2} failed",
					tInterface, map [tInterface], tClass
				));
			}
			Mapping mapping = new Mapping (tInterface, tClass, MappingType.Singleton);
			mapping.Item = Activator.CreateInstance (tClass);
			map [tInterface] = mapping;

			// Launch anticipant check
			IContextLaunchObserver anticipant = mapping.Item as IContextLaunchObserver;
			if (anticipant != null)
			{
				if (isLaunched)
				{
					InjectDependencies(anticipant);
					anticipant.OnContextLaunched();
				}
				else
				{
					if (launchAnticipants == null)
					{
						launchAnticipants = new List<IContextLaunchObserver>();
					}
					launchAnticipants.Add(anticipant);
				}
			}

			return (I)mapping.Item;
		}

		public T Resolve<T> ()
		{
			T res = default(T);
			res = (T)Resolve (typeof(T));
			return res;
		}

		public object Resolve (Type t)
		{
			object res = null;
			Mapping mapping;
			if (map.TryGetValue (t, out mapping)) {
				switch (mapping.Type) {
				case MappingType.Singleton:
					res = mapping.Item;
					break;
				}
			} else {
				throw new ResolveException ();
			}
			if (res == null) {
				throw new ResolveException ();
			}
			return res;
		}

		public void MapSignalToCommand<S, C> ()
			where S : class, IBaseSignal, new()
			where C : class, ICommand, new()
		{
			Type tSignal = typeof(S);
			IBaseSignal signal = null;
			if (!IsSingletonMapped (tSignal)) {
				signal = MapSingleton<S> ();
			} else {
				signal = Resolve<S> ();
			}
			CommandMapping mapping;
			if (!commandsMap.TryGetValue (signal, out mapping)) {
				mapping = new CommandMapping (signal);
				commandsMap [signal] = mapping;
			}
			Type tCommand = typeof(C);
			mapping.Bind (tCommand);
			signal.AddListener (OnMappedSignalDispatched);
		}

		public void InjectDependencies (object obj)
		{
			injector.Inject (obj);
		}

		#endregion

		// Calls on Launch.
		// Tasks:
		//   1. Perform injections into mapped singletons
		//   2. TBD...
		private void PostInitialization ()
		{
			foreach (var kvp in map) {
				Mapping mapping = kvp.Value;
				if (mapping.Item != null) {
					InjectDependencies (mapping.Item);
				}
			}
		}

		private void OnMappedSignalDispatched (IBaseSignal signal, object[] args)
		{
			CommandMapping cmdMap = commandsMap [signal];
			foreach (Type cmdType in cmdMap.MappedCommands) {
				ICommand cmd = commandsPool.Get(cmdType);
				// 1. Inject context values without exception, because some values may come not from Context but from Signal
				injector.Inject (cmd, false);
				// 2. Inject signal values (may override 1.)
				if (args != null) {
					injector.Inject (cmd, args);
				}
				if (cmd.IsPoolable)
				{
					cmd.OnReleased += OnPoolableCommandReleased;
				}
				cmd.Execute();
				if (!cmd.IsRetained)
				{
					cmd.Release();
				}
			}
		}

		private void OnPoolableCommandReleased(ICommand command)
		{
			command.OnReleased -= OnPoolableCommandReleased;
			commandsPool.Release(command);
		}

		private bool IsSingletonMapped (Type t)
		{
			return map.ContainsKey (t);
		}

		private object CreateInstance (Type type)
		{
			return Activator.CreateInstance (type);
		}
	}
}
