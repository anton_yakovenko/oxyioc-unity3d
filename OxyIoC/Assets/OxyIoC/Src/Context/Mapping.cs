﻿using System;

namespace OxyIoC
{
	public class Mapping
	{
		public Mapping (Type what, Type to, MappingType mappingType)
		{
			Type = mappingType;
			What = what;
			To = to;
		}

		public MappingType Type {
			get;
			private set;
		}

		public Type What { get; private set; }

		public Type To { get; private set; }
	
		// Used for singletons
		public object Item { get; set; }
	}
}

