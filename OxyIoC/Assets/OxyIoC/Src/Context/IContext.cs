﻿using System;
using OxyIoC.Signals;

namespace OxyIoC
{
	public delegate void ContextInitializationHandler (IContext context);
	
	public interface IContext : IMappingResolver
	{
		// Call this when initialization is complete
		void Launch ();
	
		// Every time tClass is requested a single instance of tClass is returned
		// Throw an Exception if T is already mapped
		T MapSingleton<T> ()
			where T : class, new();
	
		// Every time tInterface is requested a single instance of tClass is returned
		// Throw an Exception if I is already mapped
		I MapSingleton<I, C> ()
				where I : class
				where C : class, I, new();

		void MapSignalToCommand<S, C> ()
			where S : class, IBaseSignal, new()
			where C : class, ICommand, new();
	
	
		// Scans obj for [Inject] attributes and resolves injections
		void InjectDependencies (object obj);
	}
}
