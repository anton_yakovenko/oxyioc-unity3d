﻿using System;

namespace OxyIoC
{
	public interface IContextInitializer
	{
		void Initialize (IContext context);
	}
}

