﻿using System;
using System.Collections.Generic;
using OxyIoC.Signals;

namespace OxyIoC
{
	public class CommandMapping
	{
		private List<Type> commandsTypes;
		private IBaseSignal signal;

		public CommandMapping (IBaseSignal signal)
		{
			commandsTypes = new List<Type> ();
			this.signal = signal;
			SignalType = signal.GetType ();
			ValidateSignalType ();
		}

		private void ValidateSignalType ()
		{
			List<Type> types = signal.GetTypes ();
			int typesCount = types.Count;
			for (int i = 0; i < typesCount - 1; i++) {
				for (int j = i + 1; j < typesCount; j++) {
					if (types [i] == types [j]) {
						throw new CommandMappingException (String.Format (
							"Signal '{0}' has more than one parameter of type '{1}'",
							SignalType, types [i]
						));
					}
				}
			}
		}

		public Type SignalType {
			get;
			private set;
		}

		public void Bind (Type commandType)
		{
			if (!commandsTypes.Contains (commandType)) {
				commandsTypes.Add (commandType);
			}
		}

		public Type[] MappedCommands {
			get {
				return commandsTypes.ToArray ();
			}
		}
	}
}

