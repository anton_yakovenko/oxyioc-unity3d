﻿using System;
using System.Collections.Generic;

namespace OxyIoC
{
	public class CommandsPool
	{
		private Dictionary<Type, bool> poolableCheck;
		private Dictionary<Type, Stack<ICommand>> pool;

		public CommandsPool ()
		{
			poolableCheck = new Dictionary<Type, bool>();
			pool = new Dictionary<Type, Stack<ICommand>>();
		}

		public ICommand Get(Type commandType)
		{
			bool isPoolable = false;
			ICommand cmd;
			if (!poolableCheck.TryGetValue(commandType, out isPoolable))
			{
				// this means the command of this type is requested for the first time
				cmd = (ICommand)Activator.CreateInstance(commandType);
				isPoolable = cmd.IsPoolable;
				poolableCheck[commandType] = isPoolable;
				if (isPoolable)
				{
					// this guarantees that we'll allways have pool[commandType] record
					pool[commandType] = new Stack<ICommand>();
				}
			}
			else
			{
				if (isPoolable)
				{
					// Try to get command instance from pool
					Stack<ICommand> instances = pool[commandType];
					if (instances.Count > 0)
					{
						cmd = instances.Pop();
					}
					else
					{
						cmd = (ICommand)Activator.CreateInstance(commandType);
					}
				}
				else
				{
					cmd = (ICommand)Activator.CreateInstance(commandType);
				}
			}
			return cmd;
		}

		public void Release(ICommand command)
		{
			if (command.IsPoolable)
			{
				Stack<ICommand> cmdStack;
				Type cmdType = command.GetType();
				if (!pool.TryGetValue(cmdType, out cmdStack))
				{
					cmdStack = new Stack<ICommand>();
					pool[cmdType] = cmdStack;
				}
				cmdStack.Push(command);
			}
		}
	}
}

