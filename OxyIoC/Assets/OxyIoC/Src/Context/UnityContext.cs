﻿using System;
using UnityEngine;
using System.Linq;
using OxyIoC.Signals;

namespace OxyIoC
{
	public class UnityContext : MonoBehaviour, IContext
	{
		private static UnityContext Instance = null;
	
		private IContext context = null;

		private void Awake ()
		{
			if (Instance != null) {
				GameObject.Destroy (this);
			} else {
				Instance = this;
				GameObject.DontDestroyOnLoad (gameObject);
				Initialize ();
			}
		}

		private void Initialize ()
		{
			context = new Context ();
			IContextInitializer[] initializers = gameObject.GetComponents<IContextInitializer> ();
			if (initializers == null || initializers.Length == 0) {
				Debug.LogWarning ("Context is not initialized: IContextInitializer instanses are not found");
			} else {
				foreach (IContextInitializer initializer in initializers) {
					initializer.Initialize (context);
				}
			}
			context.Launch ();
		}
		#region IContext implementation

		public void Launch ()
		{
			context.Launch ();
		}

		public T MapSingleton<T> () where T : class, new()
		{
			return context.MapSingleton<T> ();
		}

		public I MapSingleton<I, C> () where I : class where C : class, I, new()
		{
			return context.MapSingleton<I, C> ();
		}

		public void MapSignalToCommand<S, C> () 
			where S : class, IBaseSignal, new() 
			where C : class, ICommand, new()
		{
			context.MapSignalToCommand<S, C> ();
		}

		public void InjectDependencies (object obj)
		{
			context.InjectDependencies (obj);
		}

		#endregion

		#region IMappingResolver implementation

		public T Resolve<T> ()
		{
			return context.Resolve<T> ();
		}

		public object Resolve (Type t)
		{
			return context.Resolve (t);
		}

		#endregion
	}
}

