﻿namespace OxyIoC
{
	public abstract class AbstractModule : IContextLaunchObserver
	{
		#region IContextLaunchAnticipant implementation
		public abstract void OnContextLaunched ();
		#endregion
	}
}

