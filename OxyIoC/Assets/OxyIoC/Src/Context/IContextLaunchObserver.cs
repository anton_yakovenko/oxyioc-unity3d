﻿namespace OxyIoC
{
	public interface IContextLaunchObserver
	{
		void OnContextLaunched();
	}
}

