﻿using System;


namespace OxyIoC
{
	public interface IMappingResolver
	{
		// Return mapped instance or throw an exception if type is not mapped
		T Resolve<T> ();

		object Resolve (Type t);
	}
}
