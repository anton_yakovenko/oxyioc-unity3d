﻿using System;

namespace OxyIoC
{
	public abstract class Command : ICommand
	{
		public abstract void Execute ();

		public virtual bool IsPoolable 
		{ 
			get 
			{
				return true;
			}
		}

		#region Internal Logic
		public event CommandReleasedHandler OnReleased;
		public bool IsRetained
		{
			get; private set;
		}

		protected void Retain()
		{
			IsRetained = true;
		}

		public void Release()
		{
			if (IsRetained)
			{
				IsRetained = false;
				if (OnReleased != null)
				{
					OnReleased.Invoke(this);
					OnReleased = null;
				}
			}
			CleanUp();
		}

		// Override to clean-up used resources
		protected virtual void CleanUp()
		{
		}
		#endregion
	}
}

