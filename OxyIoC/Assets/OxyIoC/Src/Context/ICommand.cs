﻿using System;

namespace OxyIoC
{
	public delegate void CommandReleasedHandler(ICommand command);

	public interface ICommand
	{
		void Execute();
		bool IsPoolable { get; }
		bool IsRetained { get; }
		event CommandReleasedHandler OnReleased;
		void Release();
	}
}

