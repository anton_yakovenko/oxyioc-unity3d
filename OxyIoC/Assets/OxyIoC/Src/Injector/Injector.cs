﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace OxyIoC
{
	public class Injector
	{
		private IMappingResolver resolver;
		private Dictionary<Type, List<PropertyInfo>> injectionCache;

		public Injector(IMappingResolver resolver)
		{
			this.resolver = resolver;
			injectionCache = new Dictionary<Type, List<PropertyInfo>>();
		}

		private List<PropertyInfo> GetInjectableProps(object instance)
		{
			Type type = instance.GetType();
			List<PropertyInfo> cachedProps;
			if (injectionCache.TryGetValue(type, out cachedProps))
			{
				if (cachedProps == null)
				{
					// Type is already cached and does not contain injections
					return null;
				}
			}
			else
			{
				PropertyInfo[] props = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
				if (props != null)
				{
					foreach (PropertyInfo prop in props)
					{
						object[] attrs = prop.GetCustomAttributes(true);
						foreach (object attr in attrs)
						{
							Inject injectAttr = attr as Inject;
							if (injectAttr != null)
							{
								if (cachedProps == null)
								{
									cachedProps = new List<PropertyInfo>();
								}
								cachedProps.Add(prop);
							}
						}
					}
				}
				injectionCache[type] = cachedProps;
			}
			return cachedProps;
		}

		public void Inject(object instance, bool shouldThrowException = true)
		{
			List<PropertyInfo> cachedProps = GetInjectableProps(instance);
			if (cachedProps == null)
			{
				return;
			}
			foreach (PropertyInfo prop in cachedProps)
			{
				try
				{
					object value = resolver.Resolve(prop.PropertyType);
					prop.SetValue(instance, value, null);
				}
				catch (Exception ex) 
				{
					if (shouldThrowException)
					{
						throw new InjectionException(String.Format("Injection Error: Can't inject '{0}' into '{1}'",
							prop.PropertyType, instance.GetType()), ex);
					}
				}
			}
		}

		public void Inject(object instance, object[] args)
		{
			List<PropertyInfo> cachedProps = GetInjectableProps(instance);
			if (cachedProps == null)
			{
				return;
			}
			foreach (object val in args)
			{
				Type valType = val.GetType();
				foreach (PropertyInfo prop in cachedProps)
				{
					if (prop.PropertyType == valType)
					{
						prop.SetValue(instance, val, null);
					}
				}
			}
		}
	}
}
