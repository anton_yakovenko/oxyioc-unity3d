﻿using System;

[AttributeUsage(AttributeTargets.Property,
				AllowMultiple = false,
				Inherited = true)]
public class Inject : Attribute
{
	public Inject() {}
}
