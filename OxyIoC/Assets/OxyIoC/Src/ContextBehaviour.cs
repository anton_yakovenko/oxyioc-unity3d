﻿using System;
using System.Collections.Generic;
using UnityEngine;
using OxyIoC.Signals;

namespace OxyIoC
{
	public class ContextBehaviour : MonoBehaviour
	{
		public const string CONTEXT_TAG = "Context";

		private static IContext s_context = null;

		// Used to start initialization process only
		private static bool s_TriedToInitialize = false;

		// All initialization stuff should be made in Start, since UnityContext is initialized in Awake
		private void Start()
		{
			if (s_context == null && !s_TriedToInitialize)
			{
				s_TriedToInitialize = true;
				// Try to find Context
				// 1. Fastest way: check current GameObject
				IContext ctx = GetComponent<IContext>();
				if (ctx != null)
				{
					s_context = ctx;
				}
				else
				{
					Debug.Log("Trying to find Context in current GameObject components");
					IContext[] contexts = GetComponentsInChildren<IContext>();
					if (contexts.Length > 1)
					{
						Debug.LogWarningFormat("More than one IContext is found on Scene! Choosing the first one");
					}
					else if (contexts.Length == 1)
					{
						s_context = contexts[0];
					}
					else
					{
						Debug.Log("Trying to find object with Tag 'Context'");
						GameObject[] objectsWithContextTag = null;
						try 
						{
							objectsWithContextTag = GameObject.FindGameObjectsWithTag(CONTEXT_TAG);
							if (objectsWithContextTag.Length > 1) {
								Debug.LogWarningFormat ("There are more than one object with tag {0}.", CONTEXT_TAG);
							}
						}
						catch (Exception ex)
						{
							Debug.LogWarningFormat("Could not find objects with tag: {0}. Reason: {1}",
								CONTEXT_TAG, ex.Message
							);
						}
						if (objectsWithContextTag != null && objectsWithContextTag.Length > 0)
						{
							foreach (GameObject go in objectsWithContextTag)
							{
								ctx = go.GetComponent<IContext>();
								if (ctx != null)
								{
									Debug.LogFormat(go, "Context found on '{0}'", go.name);
									s_context = ctx;
									break;
								}
							}
						}
					}

					if (s_context == null)
					{
						Debug.LogWarning("No contexts are found on Scene.");
					}
				}
			}
			if (s_context != null) {
				s_context.InjectDependencies(this);
				ContextStart ();
			}
		}

		public virtual void ContextStart()
		{
		}
	}
}