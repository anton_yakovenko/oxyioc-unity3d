﻿using System;

namespace OxyIoC
{
	public class IncorrectMappingException : Exception
	{
		public IncorrectMappingException (string message = null) : base (message)
		{
		}
	}
}

