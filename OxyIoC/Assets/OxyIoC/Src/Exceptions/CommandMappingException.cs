﻿using System;

namespace OxyIoC
{
	public class CommandMappingException : Exception
	{
		public CommandMappingException (string message = null) : base (message)
		{
		}
	}
}

