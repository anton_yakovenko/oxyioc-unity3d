﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OxyIoC;
using OxyIoC.Signals;

[TestFixture]
public class SignalsTests
{
	[Test]
	public void OneTypeSignalDispatch()
	{
		string sentMessage = "Test Message";
		var signal = new Signal<string>();
		string receivedMessage = string.Empty;
		Action<string> callback = (string message) => {
			receivedMessage = message;
		};
		signal.AddListener(callback);
		signal.Dispatch(sentMessage);
		Assert.AreEqual(sentMessage, receivedMessage);
	}

	[Test]
	public void TwoTypesSignalDispatch()
	{
		var signal = new Signal<string, int>();

		string expectedString = "Test Message";
		int expectedInt = 1;

		string actualString = string.Empty;
		int actualInt = 0;

		Action<string, int> callback = (string strVal, int intVal) => {
			actualString = strVal;
			actualInt = intVal;
		};
		signal.AddListener(callback);
		signal.Dispatch(expectedString, expectedInt);
		Assert.AreEqual(expectedString, actualString);
		Assert.AreEqual(expectedInt, actualInt);
	}

	[Test]
	public void ThreeTypesSignalDispatch()
	{
		var signal = new Signal<string, int, float>();

		string expectedString = "Test Message";
		int expectedInt = 1;
		float expectedFloat = 3.14f;

		string actualString = string.Empty;
		int actualInt = 0;
		float actualFloat = 0;

		Action<string, int, float> callback = (string strVal, int intVal, float floatVal) => {
			actualString = strVal;
			actualInt = intVal;
			actualFloat = floatVal;
		};
		signal.AddListener(callback);
		signal.Dispatch(expectedString, expectedInt, expectedFloat);

		Assert.AreEqual(expectedString, actualString);
		Assert.AreEqual(expectedInt, actualInt);
		Assert.AreEqual(expectedFloat, actualFloat);
	}

	[Test]
	public void FourTypesSignalDispatch()
	{
		var signal = new Signal<string, int, float, bool>();

		string expectedString = "Test Message";
		int expectedInt = 1;
		float expectedFloat = 3.14f;
		bool expectedBool = true;

		string actualString = string.Empty;
		int actualInt = 0;
		float actualFloat = 0;
		bool actualBool = false;

		Action<string, int, float, bool> callback = (string strVal, int intVal, float floatVal, bool boolVal) => {
			actualString = strVal;
			actualInt = intVal;
			actualFloat = floatVal;
			actualBool = boolVal;
		};

		signal.AddListener(callback);
		signal.Dispatch(expectedString, expectedInt, expectedFloat, expectedBool);

		Assert.AreEqual(expectedString, actualString);
		Assert.AreEqual(expectedInt, actualInt);
		Assert.AreEqual(expectedFloat, actualFloat);
		Assert.AreEqual(expectedBool, actualBool);
	}

	[Test]
	public void RemoveListener()
	{
		string sentMessage = "Test Message";
		var signal = new Signal<string>();
		string receivedMessage = string.Empty;
		Action<string> callback = (string message) => {
			receivedMessage = message;
		};
		signal.AddListener(callback);
		signal.RemoveListener(callback);
		signal.Dispatch(sentMessage);
		Assert.AreEqual(string.Empty, receivedMessage);
	}

	[Test]
	public void DuplicatedAddListener()
	{
		string sentMessage = "Test Message";
		var signal = new Signal<string>();
		int callsCount = 0;
		Action<string> callback = (string message) => {
			callsCount++;
		};
		signal.AddListener(callback);
		signal.AddListener(callback);
		signal.Dispatch(sentMessage);
		Assert.AreEqual(1, callsCount);
	}

	[Test]
	public void OnceListener()
	{
		var signal = new Signal<string>();
		int callsCount = 0;
		Action<string> callback = (string message) => {
			callsCount++;
		};
		signal.AddOnce(callback);
		signal.Dispatch(null);
		signal.Dispatch(null);
		Assert.AreEqual(1, callsCount);
	}

	[Test]
	[Description("Signal without listeners should not crash on Dispatch")]
	public void NoListenersDispatch()
	{
		var signal = new Signal<object>();
		signal.Dispatch(null);
	}

	[Test]
	public void GetTypes()
	{
		var signal1 = new Signal<int>();
		List<Type> types = signal1.GetTypes();
		Assert.AreEqual(1, types.Count);
		Assert.AreEqual(typeof(int), types[0]);

		// Check that signal1.GetTypes() is immutable
		types.Add(typeof(string));
		Assert.AreEqual(1, signal1.GetTypes().Count);
		Assert.AreEqual(typeof(int), signal1.GetTypes()[0]);

		var signal2 = new Signal<int, float>();
		types = signal2.GetTypes();
		Assert.AreEqual(2, types.Count);
		Assert.AreEqual(typeof(int), types[0]);
		Assert.AreEqual(typeof(float), types[1]);

		var signal3 = new Signal<int, float, string>();
		types = signal3.GetTypes();
		Assert.AreEqual(3, types.Count);
		Assert.AreEqual(typeof(int), types[0]);
		Assert.AreEqual(typeof(float), types[1]);
		Assert.AreEqual(typeof(string), types[2]);

		var signal4 = new Signal<int, float, string, bool>();
		types = signal4.GetTypes();
		Assert.AreEqual(4, types.Count);
		Assert.AreEqual(typeof(int), types[0]);
		Assert.AreEqual(typeof(float), types[1]);
		Assert.AreEqual(typeof(string), types[2]);
		Assert.AreEqual(typeof(bool), types[3]);
	}
}

