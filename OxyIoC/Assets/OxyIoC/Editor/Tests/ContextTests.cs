﻿using UnityEngine;
using System;
using System.Collections;
using NUnit.Framework;
using OxyIoC;

[TestFixture]
public class ContextTests 
{
	[Test]
	public void MapSingleton()
	{
		Context context = new Context();
		context.MapSingleton<Model1>();
		Model1 model1 = (Model1)context.Resolve(typeof(Model1));
		Assert.NotNull(model1);
	}

	[Test]
	public void MapInterfaceToSingleton()
	{
		Context context = new Context();
		context.MapSingleton<IModel, Model1>();
		IModel model1 = (IModel)context.Resolve(typeof(IModel));
		Assert.NotNull(model1);
		IModel model2 = (IModel)context.Resolve(typeof(IModel));
		Assert.AreSame(model1, model2);
		Assert.AreEqual(typeof(Model1), model1.GetType());
	}

	[Test]
	[ExpectedException(typeof(IncorrectMappingException))]
	public void DuplicatedSingletonMapping()
	{
		Context context = new Context();
		context.MapSingleton<IModel, Model1>();
		context.MapSingleton<IModel, Model2>();
	}

	[Test]
	[ExpectedException(typeof(ResolveException))]
	public void ResolveUnmappedType()
	{
		Context context = new Context();
		context.Resolve(typeof(IModel));
	}

	[Test]
	public void InjectDependencies()
	{
		Context context = new Context();
		context.MapSingleton<IModel, Model1>();

		IModel model = context.Resolve<IModel>();

		InjectionAcceptor acceptor1 = new InjectionAcceptor();
		context.InjectDependencies(acceptor1);
		Assert.AreSame(model, acceptor1.Model);

		InjectionAcceptor acceptor2 = new InjectionAcceptor();
		context.InjectDependencies(acceptor2);
		Assert.AreSame(model, acceptor2.Model);
	}

	[Test]
	public void LaunchSubscribsion()
	{
		Context context = new Context();
		int callsCount = 0;
		ContextInitializationHandler callback = (IContext obj) => {
			callsCount++;
		};
		context.OnInitialized += callback;
		context.OnInitialized += callback;

		Assert.AreEqual(0, callsCount);

		context.Launch();
		Assert.AreEqual(1, callsCount);

		context.OnInitialized += callback;
		Assert.AreEqual(2, callsCount);
	}

	[Test]
	public void PostLaunchAndAnticpantsTest()
	{
		IContext context = new Context();
		LaunchAnticipantA anticipantA = (LaunchAnticipantA)context.MapSingleton<ILaunchAnticipantA, LaunchAnticipantA>();
		LaunchAnticipantB anticipantB = (LaunchAnticipantB)context.MapSingleton<ILaunchAnticipantB, LaunchAnticipantB>();

		Assert.IsFalse(anticipantA.isLaunchObserved);
		Assert.IsFalse(anticipantB.isLaunchObserved);

		context.Launch();

		Assert.IsTrue(anticipantA.isLaunchObserved);
		Assert.IsTrue(anticipantB.isLaunchObserved);

		Assert.AreEqual(anticipantA, anticipantB.anticipantA);
		Assert.AreEqual(anticipantB, anticipantA.anticipantB);

		// Post-launch mapping
		LaunchAnticipantC anticipantC = (LaunchAnticipantC)context.MapSingleton<ILaunchAnticipantC, LaunchAnticipantC>();
		Assert.AreEqual(anticipantA, anticipantC.anticipantA);
		Assert.AreEqual(anticipantB, anticipantC.anticipantB);
		Assert.IsTrue(anticipantC.isLaunchObserved);
	}

	public interface IModel {}
	public class Model1 : IModel {}
	public class Model2 : IModel {}
	public class NotCastableType {}

	public class InjectionAcceptor
	{
		[Inject]
		public IModel Model { get; set; }
	}

	public interface ILaunchAnticipantA : IContextLaunchObserver { }
	public class LaunchAnticipantA : ILaunchAnticipantA
	{
		[Inject]
		public ILaunchAnticipantB anticipantB { get; set; }

		public bool isLaunchObserved = false;

		public void OnContextLaunched()
		{
			Assert.NotNull(anticipantB);
			isLaunchObserved = true;
		}
	}

	public interface ILaunchAnticipantB : IContextLaunchObserver { }
	public class LaunchAnticipantB : ILaunchAnticipantB
	{
		[Inject]
		public ILaunchAnticipantA anticipantA { get; set; }

		public bool isLaunchObserved = false;

		public void OnContextLaunched()
		{
			Assert.NotNull(anticipantA);
			isLaunchObserved = true;
		}
	}

	public interface ILaunchAnticipantC : IContextLaunchObserver { }
	public class LaunchAnticipantC : ILaunchAnticipantC
	{
		[Inject]
		public ILaunchAnticipantA anticipantA { get; set; }

		[Inject]
		public ILaunchAnticipantB anticipantB { get; set; }

		public bool isLaunchObserved = false;

		public void OnContextLaunched()
		{
			Assert.NotNull(anticipantA);
			Assert.NotNull(anticipantB);
			isLaunchObserved = true;
		}
	}
}
