﻿using UnityEngine;
using UnityEditor;
using OxyIoC;
using NUnit.Framework;

[TestFixture]
public class CommandsPoolTests
{
	[Test]
	public void GetPoolableCommand()
	{
		PoolableCommand.TotalInstancesCount = 0;
		CommandsPool pool = new CommandsPool();
		ICommand poolableCommand1 = pool.Get(typeof(PoolableCommand));
		Assert.NotNull(poolableCommand1);
		ICommand poolableCommand2 = pool.Get(typeof(PoolableCommand));
		Assert.NotNull(poolableCommand2);
		Assert.AreNotSame(poolableCommand1, poolableCommand2);
		pool.Release(poolableCommand1);
		ICommand poolableCommand3 = pool.Get(typeof(PoolableCommand));
		Assert.NotNull(poolableCommand3);
		Assert.AreSame(poolableCommand1, poolableCommand3);
		Assert.AreEqual(2, PoolableCommand.TotalInstancesCount);
	}

	[Test]
	public void GetNotPoolableCommand()
	{
		NotPoolableCommand.TotalInstancesCount = 0;
		CommandsPool pool = new CommandsPool();
		ICommand notPoolableCommand1 = pool.Get(typeof(NotPoolableCommand));
		Assert.NotNull(notPoolableCommand1);
		ICommand notPoolableCommand2 = pool.Get(typeof(NotPoolableCommand));
		Assert.NotNull(notPoolableCommand2);
		Assert.AreNotSame(notPoolableCommand1, notPoolableCommand2);
		pool.Release(notPoolableCommand1);
		ICommand notPoolableCommand3 = pool.Get(typeof(NotPoolableCommand));
		Assert.NotNull(notPoolableCommand3);
		Assert.AreNotSame(notPoolableCommand1, notPoolableCommand3);
		Assert.AreEqual(3, NotPoolableCommand.TotalInstancesCount);
	}

	#region Test Commands
	public class PoolableCommand: Command
	{
		public static int TotalInstancesCount = 0;

		public PoolableCommand()
		{
			TotalInstancesCount++;
		}

		#region ICommand implementation
		public override void Execute ()
		{
		}

		public override bool IsPoolable {
			get {
				return true;
			}
		}
		#endregion
	}

	public class NotPoolableCommand: Command
	{
		public static int TotalInstancesCount = 0;

		public NotPoolableCommand()
		{
			TotalInstancesCount++;
		}

		#region ICommand implementation
		public override void Execute ()
		{
		}

		public override bool IsPoolable {
			get {
				return false;
			}
		}
		#endregion
	}
	#endregion
}
