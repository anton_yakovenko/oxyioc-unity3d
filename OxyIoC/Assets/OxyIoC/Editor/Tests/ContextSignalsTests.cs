﻿using System;
using NUnit.Framework;
using OxyIoC;
using OxyIoC.Signals;
using UnityEngine;

[TestFixture]
public class ContextSignalsTests
{
	[Test]
	public void MapSignal()
	{
		Context context = new Context();
		context.MapSingleton<DataChangedSignal>();

		int passedArg = 10;
		int receivedArg = 0;

		DataChangedSignal contextSignal = (DataChangedSignal)context.Resolve(typeof(DataChangedSignal));
		contextSignal.AddListener((int arg1) => {
			receivedArg = arg1;
		});

		contextSignal = (DataChangedSignal)context.Resolve(typeof(DataChangedSignal));
		contextSignal.Dispatch(passedArg);

		Assert.AreEqual(passedArg, receivedArg);
	}

	[Test]
	public void MapSignalToCommand()
	{
		Context context = new Context();
		// Initialize context
		context.MapSignalToCommand<DataChangedSignal, SaveDataCommand>();
		context.MapSingleton<IModel, Model>();

		DataChangedSignal signal = context.Resolve<DataChangedSignal>();
		int sentValue = 10;
		signal.Dispatch(sentValue);
		IModel model = context.Resolve<IModel>();
		Assert.AreEqual(sentValue, model.Data);
	}

	[Test]
	public void RetainedPooledCommandExecution()
	{
		Context context = new Context();
		RetainedPoolableCommand.TotalInstancesCount = 0;
		// Initialize context
		context.MapSignalToCommand<RetainedCommandSignal, RetainedPoolableCommand>();
		IModel model = context.MapSingleton<IModel, Model>();
		RetainedCommandSignal signal = context.Resolve<RetainedCommandSignal>();
		int sentValue = 10;
		int sentValue2 = 20;
		Log("Test: Dispatch...");
		signal.Dispatch(sentValue, ((IAsyncResult ar) => {
			Log("Test: Callback. Waits...");
			ar.AsyncWaitHandle.WaitOne();
			Log("Test: Assertions...");
			Assert.AreEqual(sentValue, model.Data);
			Assert.AreEqual(1, RetainedPoolableCommand.TotalInstancesCount);

			signal.Dispatch(sentValue2, ((IAsyncResult ar2) => {
				ar2.AsyncWaitHandle.WaitOne();
				Assert.AreEqual(sentValue2, model.Data);
				Assert.AreEqual(1, RetainedPoolableCommand.TotalInstancesCount); // since it is poolable command
			}));
		}));
	}

	[Test]
	public void RetainedNotPooledCommandExecution()
	{
		Context context = new Context();
		RetainedNotPullableCommand.TotalInstancesCount = 0;
		// Initialize context
		context.MapSignalToCommand<RetainedCommandSignal, RetainedNotPullableCommand>();
		IModel model = context.MapSingleton<IModel, Model>();
		RetainedCommandSignal signal = context.Resolve<RetainedCommandSignal>();
		int sentValue = 10;
		int sentValue2 = 20;
		Log("Test: Dispatch...");
		signal.Dispatch(sentValue, ((IAsyncResult ar) => {
			Log("Test: Callback. Waits...");
			ar.AsyncWaitHandle.WaitOne();
			Log("Test: Assertions...");
			Assert.AreEqual(sentValue, model.Data);
			Assert.AreEqual(1, RetainedPoolableCommand.TotalInstancesCount);

			signal.Dispatch(sentValue2, ((IAsyncResult ar2) => {
				ar2.AsyncWaitHandle.WaitOne();
				Assert.AreEqual(sentValue2, model.Data);
				Assert.AreEqual(2, RetainedPoolableCommand.TotalInstancesCount); // since it is NOT poolable command
			}));
		}));
	}

	public class DataChangedSignal : Signal<int>
	{
	}

	public class SaveDataCommand : Command
	{
		[Inject]
		public int ChangedData { get; set; }

		[Inject]
		public IModel Model { get; set; }

		public override void Execute ()
		{
			Model.Data = ChangedData;
		}
	}

	public interface IModel
	{
		int Data { get; set; }
	}

	public class Model : IModel
	{
		public int Data { get; set; }
	}

	#region Retained Command testclasses
	public class RetainedCommandSignal : Signal<int, Action<IAsyncResult>> {}

	public class RetainedPoolableCommand : Command
	{
		public static int TotalInstancesCount = 0;

		public RetainedPoolableCommand()
		{
			TotalInstancesCount++;
		}

		[Inject]
		public Action<IAsyncResult> ExecuteStartCallback { get; set; }

		[Inject]
		public int value { get; set; }

		[Inject]
		public IModel model { get; set; }

		private Action asyncAction;

		public override void Execute ()
		{
			Retain();
			Log("> RetainedCommand: Start Execution");
			asyncAction = () => {
				Log("> RetainedCommand: sleep...");
				System.Threading.Thread.Sleep(500);
				Log("> RetainedCommand: Change data");
				model.Data = value;
			};
			Log("> RetainedCommand: Begin Invoke");
			IAsyncResult ar = asyncAction.BeginInvoke(EndInvokeCallback, null);
			Log("> RetainedCommand: ExecuteStartCallback");
			ExecuteStartCallback.Invoke(ar);
		}

		private void EndInvokeCallback(IAsyncResult ar)
		{
			Release();
			Log("> RetainedCommand: EndInvokeCallback");
			asyncAction.EndInvoke(ar);
			Log("> RetainedCommand: Release");
		}

		public override bool IsPoolable {
			get {
				return true;
			}
		}
	}

	public class RetainedNotPullableCommand : RetainedPoolableCommand
	{
		public new static int TotalInstancesCount = 0;

		public RetainedNotPullableCommand()
		{
			TotalInstancesCount++;
		}

		public override bool IsPoolable {
			get {
				return false;
			}
		}
	}
	#endregion
	private static void Log(string msg)
	{
		UnityEngine.Debug.Log(msg);
	}
}
