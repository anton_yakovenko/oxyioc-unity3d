﻿using UnityEngine;
using System;
using System.Collections;
using NUnit.Framework;
using OxyIoC;

[TestFixture]
public class InjectorTests 
{
	[Test]
	public void Inject()
	{
		IMappingResolver resolver = new Resolver();
		Injector injector = new Injector(resolver);
		CorrectAcceptor acceptor = new CorrectAcceptor();
		injector.Inject(acceptor);
		Assert.NotNull(acceptor.InjectedValue);
	}

	[Test]
	[ExpectedException(typeof(InjectionException))]
	public void NotMappedInjection()
	{
		IMappingResolver resolver = new Resolver();
		Injector injector = new Injector(resolver);
		IncorrectAcceptor acceptor = new IncorrectAcceptor();
		injector.Inject(acceptor);
	}

	[Test]
	public void NotMappedInjectionWithoutException()
	{
		IMappingResolver resolver = new Resolver();
		Injector injector = new Injector(resolver);
		IncorrectAcceptor acceptor = new IncorrectAcceptor();
		injector.Inject(acceptor, false);
	}

	[Test]
	public void RepeatedInjections()
	{
		IMappingResolver resolver = new Resolver();
		Injector injector = new Injector(resolver);

		CorrectAcceptor acceptor1 = new CorrectAcceptor();
		injector.Inject(acceptor1);
		Assert.NotNull(acceptor1.InjectedValue);

		CorrectAcceptor acceptor2 = new CorrectAcceptor();
		injector.Inject(acceptor2);
		Assert.NotNull(acceptor2.InjectedValue);
	}

	[Test]
	public void InjectorOnTypeWithoutInjections()
	{
		IMappingResolver resolver = new Resolver();
		Injector injector = new Injector(resolver);

		NotAcceptor notAcceptor = new NotAcceptor();
		injector.Inject(notAcceptor);
		Assert.IsNull(notAcceptor.Model);
	}

	[Test]
	public void InjectSpecifiedValues()
	{
		string strVal = "String Value";
		int intVal = 123;
		Injector injector = new Injector(null);

		ValuesAcceptor acceptor = new ValuesAcceptor();
		injector.Inject(acceptor, new object[] { strVal, intVal });
		Assert.AreEqual(strVal, acceptor.StrProp);
		Assert.AreEqual(intVal, acceptor.IntProp);

		// Caching test
		acceptor = new ValuesAcceptor();
		injector.Inject(acceptor, new object[] { strVal, intVal });
		Assert.AreEqual(strVal, acceptor.StrProp);
		Assert.AreEqual(intVal, acceptor.IntProp);
	}

	#region Test context classes
	class Resolver : IMappingResolver
	{
		public T Resolve<T> ()
		{
			T res = default(T);
			res = (T)Resolve(typeof(T));
			return res;
		}

		public object Resolve(Type type)
		{
			if (type == typeof(IModel))
			{
				return new Model();
			}
			else
			{
				throw new ResolveException();
			}
		}
	}

	public interface IModel {}
	public class Model : IModel {}

	public class CorrectAcceptor
	{
		[Inject]
		public IModel InjectedValue { get; set; }
	}

	public class IncorrectAcceptor
	{
		[Inject]
		public string InjectedValue { get; set; }
	}

	public class NotAcceptor
	{
		public IModel Model { get; set; }
	}
	#endregion

	#region Injection with values class
	public class ValuesAcceptor
	{
		[Inject]
		public int IntProp {
			get;
			set;
		}

		[Inject]
		public string StrProp {
			get;
			set;
		}
	}
	#endregion
}
