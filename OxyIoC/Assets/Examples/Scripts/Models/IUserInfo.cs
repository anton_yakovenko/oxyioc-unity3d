﻿using UnityEngine;
using System.Collections;

public interface IUserInfo 
{
	string UserName { get; set; }
}
