﻿using UnityEngine;
using System.Collections;
using OxyIoC;

public class DataFetcher : AbstractModule, IDataFetcher 
{
	private const int FETCH_INTERVAL_MS = 1000;

	[Inject]
	public GameTickSignal gameTickSignal { get; set; }

	private int elapsedMS = 0;

	#region implemented abstract members of AbstractModule

	public override void OnContextLaunched ()
	{
		Debug.Log("DataFetcher: Context launched!");
		gameTickSignal.AddListener(OnTick);
	}
	#endregion

	private void OnTick(int ms)
	{
		elapsedMS += ms;
		if (elapsedMS >= FETCH_INTERVAL_MS)
		{
			Fetch();
			elapsedMS %= FETCH_INTERVAL_MS;
		}
	}

	private void Fetch()
	{
		Debug.Log("DataFetcher: Fetch");
	}
}
