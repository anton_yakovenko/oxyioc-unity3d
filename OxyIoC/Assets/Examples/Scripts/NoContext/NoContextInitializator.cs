﻿using UnityEngine;
using System.Collections;

public class NoContextInitializator : MonoBehaviour {

	[SerializeField]
	private UserNameEditor userNameEditor;

	UserInfo userInfo;
	UserNameChangedSignal userNameChangedSignal;
	SaveUserNameSignal saveUserNameSignal;

	void Awake()
	{
		userInfo = new UserInfo ();
		userNameChangedSignal = new UserNameChangedSignal ();
		saveUserNameSignal = new SaveUserNameSignal ();

		userNameEditor.userInfo = userInfo;
		userNameEditor.userNameChangedSignal = userNameChangedSignal;
		userNameEditor.saveuserNameSignal = saveUserNameSignal;

		userNameChangedSignal.AddListener ((name) => {
			Debug.LogFormat ("New name: {0}", name);
		});

		saveUserNameSignal.AddListener ((name) => {
			Debug.LogFormat ("Save Name: {0}", name);
		});

		userNameEditor.ContextStart ();
	}
}
