﻿using UnityEngine;
using OxyIoC;

public class CommandsBootstrap : MonoBehaviour, IContextInitializer 
{
	#region IContextInitializer implementation
	public void Initialize (IContext context)
	{
		context.MapSignalToCommand<UserNameChangedSignal, UserNameChangedCommand>();
		context.MapSignalToCommand<SaveUserNameSignal, SaveUserNameCommand>();
	}
	#endregion
}
