﻿using UnityEngine;
using OxyIoC;

public class ServicesBootstrap : MonoBehaviour, IContextInitializer
{
	#region IContextInitializer implementation
	public void Initialize (IContext context)
	{
		context.MapSingleton<IDataFetcher, DataFetcher>();
	}
	#endregion
}
