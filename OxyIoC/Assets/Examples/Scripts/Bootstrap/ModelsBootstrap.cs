﻿using UnityEngine;
using System.Collections;
using OxyIoC;

public class ModelsBootstrap : MonoBehaviour, IContextInitializer 
{
	#region IContextInitializer implementation
	public void Initialize (IContext context)
	{
		context.MapSingleton<IUserInfo, UserInfo>();
	}
	#endregion
}
