﻿using UnityEngine;
using OxyIoC;

public class SignalsBootstrap : MonoBehaviour, IContextInitializer 
{
	#region IContextInitializer implementation
	public void Initialize (IContext context)
	{
		context.MapSingleton<UserNameChangedSignal>();
		context.MapSingleton<GameTickSignal>();
	}
	#endregion
}
