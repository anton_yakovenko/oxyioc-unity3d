﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using OxyIoC;

public class UserNameDisplay : ContextBehaviour 
{
	#region Context
	[Inject]
	public UserNameChangedSignal userNameChangedSignal { get; set; }
	#endregion

	#region Inspector Fields
	[SerializeField]
	private Text txtUserName;
	#endregion

	public override void ContextStart ()
	{
		userNameChangedSignal.AddListener(OnUserNameChanged);
	}

	private void OnUserNameChanged(string value)
	{
		txtUserName.text = value;
	}

	private void OnDestroy()
	{
		userNameChangedSignal.RemoveListener(OnUserNameChanged);
	}
}
