﻿using UnityEngine;
using System.Collections;
using OxyIoC;
using System;

public class GameTickController : ContextBehaviour 
{
	[Inject]
	public GameTickSignal gameTickSignal { get; set; }

	private Action UpdateDelegate = delegate {};

	public override void ContextStart ()
	{
		Debug.Log("[DEBUG] GameTickController : ContextAwake");
		UpdateDelegate = Tick;
	}

	private void Tick()
	{
		gameTickSignal.Dispatch((int)(Time.deltaTime * 1000));
	}

	private void Update()
	{
		UpdateDelegate();
	}
}
