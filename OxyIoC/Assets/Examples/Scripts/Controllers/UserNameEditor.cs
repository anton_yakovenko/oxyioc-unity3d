﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using OxyIoC;

public class UserNameEditor : ContextBehaviour
{
	#region Context
	[Inject]
	public IUserInfo userInfo { get; set; }

	[Inject]
	public UserNameChangedSignal userNameChangedSignal { get; set; }

	[Inject]
	public SaveUserNameSignal saveuserNameSignal { get; set; }
	#endregion

	#region Inspector Fields
	[SerializeField]
	private InputField nameInput;

	[SerializeField]
	private Button btnOk;
	#endregion

	public override void ContextStart()
	{
		btnOk.onClick.AddListener(OnBtnOkClick);
		nameInput.onValueChanged.AddListener((string val) => {
			userNameChangedSignal.Dispatch(val);
		});
	}

	private void OnBtnOkClick()
	{
		saveuserNameSignal.Dispatch(nameInput.text);
	}

	private void OnDestroy()
	{
		btnOk.onClick.RemoveListener(OnBtnOkClick);
	}
}
