﻿using OxyIoC.Signals;

public class SaveUserNameSignal : Signal<string>
{
}
