﻿using UnityEngine;
using OxyIoC.Signals;

/// <summary>
/// Game tick signal.
///	Arguments:
///		1. deltaTimeMS: int
/// </summary>
public class GameTickSignal : Signal<int> {}
