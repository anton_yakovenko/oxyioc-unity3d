﻿using OxyIoC.Signals;

public class UserNameChangedSignal : Signal<string> {}
