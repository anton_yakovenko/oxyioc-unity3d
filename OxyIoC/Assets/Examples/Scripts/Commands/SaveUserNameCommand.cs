﻿using UnityEngine;
using System.Collections;
using OxyIoC;

public class SaveUserNameCommand : Command 
{
	[Inject]
	public string userName {
		get;
		set;
	}

	public override void Execute ()
	{
		Debug.LogFormat(">> Saving user name: {0}", userName);
	}
}
