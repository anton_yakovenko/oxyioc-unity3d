﻿using UnityEngine;
using OxyIoC;

public class UserNameChangedCommand : Command 
{
	[Inject]
	public IUserInfo userInfo {
		get;
		set;
	}

	[Inject]
	public string changedNameValue {
		get;
		set;
	}

	public override void Execute ()
	{
		Debug.LogFormat(">> user name changed to {0}", changedNameValue);
		userInfo.UserName = changedNameValue;
	}
}
